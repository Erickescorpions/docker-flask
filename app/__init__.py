from flask import Flask

# definimos una aplicacion con el nombre de la carpeta principal del proyecto
app = Flask(__name__)

# dejamos definida la importacion del archivo donde iran las rutas
from app import views