from app import app
from flask import render_template

@app.route('/')
def home():
   return "hello world!"

@app.route('/template')
def template():
   data = {
      'title': 'Template',
      'text': 'Hola mundo desde datos de python'
   }

   return render_template('hello-world.html', data=data)